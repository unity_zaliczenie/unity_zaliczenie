using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeController : MonoBehaviour
{
    public float Speed { get; set; }

    private void Update()
    {
        transform.position += Vector3.left * Speed * Time.deltaTime;    
    }
}
