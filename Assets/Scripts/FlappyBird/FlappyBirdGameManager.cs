using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlappyBirdGameManager : MonoBehaviour
{
    const string maxTimeSaveKey = "MaxTime";
    public float Timer { get; private set; }

    private int maxTimer;
    public int MaxTimer
    {
        get
        {
            if (maxTimer > Timer)
                return maxTimer;

            return (int)Timer;
        }
        internal set
        {
            maxTimer = value;
        }
    }

    private void Awake()
    {
        MaxTimer = PlayerPrefs.GetInt(maxTimeSaveKey, 0);
    }

    private void Update()
    {
        Timer += Time.deltaTime;
    }

    public void GameOver()
    {
        if ((int)Timer > PlayerPrefs.GetInt(maxTimeSaveKey, 0))
            PlayerPrefs.SetInt(maxTimeSaveKey, (int)Timer);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
