using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject cubePrefab;

    private void OnEnable()
    {
        Debug.Log(10 + 40/20);

        Test3D();
        return;

        for (int i = 0; i < 3; i++)
        {
            Debug.Log(GetFib(i));
        }

        for (int x = 0; x < 10; x++)
        {
            for (int y = 0; y < 10; y++)
            {
                var position = new Vector3(x, y, 0f);
                var spawnedRenderer = Instantiate(cubePrefab, position, Quaternion.identity, transform).GetComponent<MeshRenderer>();
                spawnedRenderer.transform.localScale = Vector3.one * 0.9f;
                spawnedRenderer.material.color = new Color(x / 5f, y / 5f, 0f, 0f);
            }
        }
    }

    private void OnDisable()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    private float GetFib(float x)
    {
        if (x == 0 || x == 1)
            return 1;

        return GetFib(x - 1) + GetFib(x);
    }

    [ContextMenu("Test3D")]
    public void Test3D()
    {
        for (int y = 0; y < 20; y++)
        {
            var pos3 = new Vector3(y, Mathf.Sin(12 * y), 0f);
            Instantiate(cubePrefab, pos3, Quaternion.identity, transform);
        }

        for (int i = 0; i < 12; i++)
        {
            var pos2 = new Vector3(i, Mathf.Sign(i), 0f);
            var pos1 = new Vector3(i, 12f * Mathf.Sign(i), 0f);

            Instantiate(cubePrefab, pos2, Quaternion.identity, transform);
            Instantiate(cubePrefab, pos1, Quaternion.identity, transform);
        }
    }
}
