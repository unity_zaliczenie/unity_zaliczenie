using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controler : MonoBehaviour
{
    private Vector3 Vec = new Vector3(0f, 0f, 0f);
    public GameObject Cube;
    private int FibNum;
    private int CounterAmmount = 1;
    private int Counter = 0;
    private int Ammount = 1;
    private float Num = 0;
    private float posX;
    private float posY;
    private float g = 1f;
    private float b = 1f;
    private float r = 1f;
    private bool Bialy = true;
    public int CubeAmmountFib = 0;
    public int CubeAmmountCircle = 0;
    public int CubeAmmountSpiral = 0;
    public int CubeAmmountSin = 0;
    public float Radius = 0;
    void Start()
    {
        for (int i = 0; i < CubeAmmountFib; i++) // zadanie 3.1
        {
            Instantiate(Cube, Vec, Quaternion.identity);
            FibNum = Fib(i);
            Num = Vec.x;
            Vec = new Vector3(Num + FibNum + 1, 0f, 0f);
        }
        for (int i = 0; i < CubeAmmountSin; i++) // zadanie 3.2
        {
            Num = Mathf.Sin(i);
            Vec = new Vector3(i, Num, 3f);
            Instantiate(Cube, Vec, Quaternion.identity);
        }
        for (int i = 0; i < CubeAmmountCircle; i++) // zadanie 3.3
        {
            Num = (360f / CubeAmmountCircle);
            posX = Radius * Mathf.Cos(Mathf.Deg2Rad * (i * Num));
            posY = Radius * Mathf.Sin(Mathf.Deg2Rad * (i * Num));

            Vec = new Vector3(posX, posY, 6f);
            Instantiate(Cube, Vec, Quaternion.identity);
        }
        Vec = new Vector3(0f, 0f ,9f);              // zadanie3.4
        for (int i = 0; i < CubeAmmountSpiral; i++)
        {
            Instantiate(Cube, Vec, Quaternion.identity);
            if (Ammount == 5)
            {
                Ammount = 1;
            }
            if(Ammount == 1)
            {
                Vec = new Vector3(Vec.x, Vec.y - 1f,Vec.z);
            }
            else if(Ammount == 2)
            {
                Vec = new Vector3(Vec.x - 1f, Vec.y, Vec.z);
            }
            else if(Ammount == 3)
            {
                Vec = new Vector3(Vec.x, Vec.y + 1f, Vec.z);
            }
            else if(Ammount == 4)
            {
                Vec = new Vector3(Vec.x + 1f, Vec.y, Vec.z);
            }
            Counter++;
            if(CounterAmmount == Counter)
            {
                Counter = 0;
                Ammount++;
                CounterAmmount++;
            }
        }
        Vec = new Vector3(0f, 0f, 12f);                 //zadanie 4.1.1
        Zad4_Local();
        Vec = new Vector3(10f, 0f, 12f);                //dalej zadanie 4.1.1, pokazuje �e to jest lokalne(nie ma zmian bo jedyna r�nica to pozycja startowa)
        Zad4_Local();
        Vec = new Vector3(0f, 0f, 15f);                 //zadanie 4.1.2
        Zad4_World();
        Vec = new Vector3(10f, 10f, 15f);               //dalej zadanie 4.1.2, pokazuje �e kolor jest zale�ny od pozycji w �wiecie
        Zad4_World();
        Vec = new Vector3(0f, 0f, 18f);                 //zadanie 4.2.1
        g = 1f;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                g -= (1f / 8f);
                var SpawnedCube = Instantiate(Cube, Vec, Quaternion.identity);
                SpawnedCube.GetComponent<MeshRenderer>().material.color = new Color(1f, g, g, 0f);
                Vec = new Vector3(Vec.x + 1, Vec.y, Vec.z);
            }
            Vec = new Vector3(Vec.x - 8, Vec.y + 1, Vec.z);
            g = 1f;
        }
        Vec = new Vector3(0f, 0f, 21f);                     //zadanie 4.2.2
        g = 1f;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                g -= (1f / 4f);
                var SpawnedCube = Instantiate(Cube, Vec, Quaternion.identity);
                SpawnedCube.GetComponent<MeshRenderer>().material.color = new Color(1f, g, g, 0f);
                Vec = new Vector3(Vec.x + 1, Vec.y, Vec.z);
            }
            g = 1f;
            for (int j = 0; j < 4; j++)
            {
                g -= (1f / 4f);
                var SpawnedCube = Instantiate(Cube, Vec, Quaternion.identity);
                SpawnedCube.GetComponent<MeshRenderer>().material.color = new Color(g, 0f, 0f, 0f);
                Vec = new Vector3(Vec.x + 1, Vec.y, Vec.z);
            }
            Vec = new Vector3(Vec.x - 8, Vec.y + 1, Vec.z);
            g = 1f;
        }
        Vec = new Vector3(0f, 0f, 24f);                     //zadanie 5.1
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                var SpawnedCube = Instantiate(Cube, Vec, Quaternion.identity);
                if (Bialy == true)
                {
                    SpawnedCube.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f, 0f);
                    Bialy = false;
                }
                else
                {
                    SpawnedCube.GetComponent<MeshRenderer>().material.color = new Color(0f, 0f, 0f, 0f);
                    Bialy = true;
                }
                Vec = new Vector3(Vec.x + 1, Vec.y, Vec.z);
            }
            Vec = new Vector3(Vec.x - 8, Vec.y + 1, Vec.z);
            Bialy = !Bialy;
        }
    }
    public void Zad4_Local()                                      
    {
        r = 1f;
        g = 1f;
        b = 1f;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                g -= (1f / 8f);
                r = g * b;
                var SpawnedCube = Instantiate(Cube, Vec, Quaternion.identity);
                SpawnedCube.GetComponent<MeshRenderer>().material.color = new Color(r, g, b, 0f);
                Vec = new Vector3(Vec.x + 1, Vec.y, Vec.z);
            }
            Vec = new Vector3(Vec.x - 8, Vec.y + 1, Vec.z);
            b -= (1f / 8f);
            g = 1f;
        }
    }
    public void Zad4_World()
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                var SpawnedCube = Instantiate(Cube, Vec, Quaternion.identity);
                float LocalR = SpawnedCube.transform.localPosition.x / 30f;
                float LocalG = SpawnedCube.transform.localPosition.y / 30f;
                float LocalB = SpawnedCube.transform.localPosition.z / 30f;
                SpawnedCube.GetComponent<MeshRenderer>().material.color = new Color(LocalR, LocalG, LocalB, 0f);
                Vec = new Vector3(Vec.x + 1, Vec.y, Vec.z);
            }
            Vec = new Vector3(Vec.x - 8, Vec.y + 1, Vec.z);
        }
    }
    public int Fib(int x)
    {
        if(x == 0 || x == 1)
        {
            return 1;
        }
        else
        {
            return Fib(x - 2) + Fib(x - 1);
        }
    }
}

